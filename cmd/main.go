package main

import (
	"gateway/internal/config"
	"gateway/internal/handlers"
	"gateway/internal/server"
	"github.com/sirupsen/logrus"
	"log"
	"net/http"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {

	logger := logrus.New()
	if logger == nil {
		log.Fatal("New logger failed")
	}

	configuration, err := config.NewConfiguration()
	if err != nil {
		logger.Fatal("config.NewConfiguration:", err.Error())
	}

	route := InitHandlers(InitHandlersDep{
		Conf:   configuration,
		Logger: logger,
	})

	srv := server.NewServer(server.Dependencies{
		Handler: route,
		Logger:  logger,
		Config:  configuration,
	})
	go func() {
		err := srv.Start()
		if err != nil {
			logger.Fatalf("failed to Start server; err - %v", err)
		}
	}()

	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)

	<-done
	logger.Error("Server Stopped")

	err = srv.Stop()
	if err != nil {
		logger.Errorf("failed to correct Stop server; err - %v", err)
	}
	time.Sleep(time.Second * 3)
	logger.Error("Server Exited Properly")
}

func InitHandlers(dep InitHandlersDep) http.Handler {

	/*requestIDMiddleware := middleware.NewRequestIDMiddleware()

	limiterMiddleware, err := middleware.NewLimiterMiddleware(middleware.LimiterMiddlewareDependencies{
		Logger: dep.Logger,
	}, dep.Conf.PortalSecurity.Limiter.RequestPerSecond, dep.Conf.PortalSecurity.Limiter.MaxOnlineRequest)
	if err != nil {
		dep.Logger.WithError(err).Fatal("Can't init limiterMiddleware")
	}

	authMiddleware, err := middleware.NewAuthMiddleware(middleware.AuthMiddlewareDependencies{
		RepositoryRegistry: dep.RepositoryRegistry,
		Conf:               dep.Conf,
	})
	if err != nil {
		dep.Logger.WithError(err).Fatal("Can't init authMiddleware")
	}

	middlewares := handlers.Middlewares{
		RequestIDMiddleware: requestIDMiddleware,
		LimiterMiddleware:   limiterMiddleware,
		AuthMiddleware:      authMiddleware,
	}*/

	redirectHandlers, err := handlers.NewRedirectHandlers(handlers.RedirectDependencies{
		Logger: dep.Logger,
		Conf:   dep.Conf,
	})
	if err != nil {
		dep.Logger.WithError(err).Fatal("Can't init redirectHandlers")

	}

	route := handlers.NewRoute(handlers.RouteDependencies{
		Logger:   dep.Logger,
		Redirect: redirectHandlers,
		Conf:     dep.Conf,
	})

	return route
}

type InitHandlersDep struct {
	Conf   *config.Config
	Logger *logrus.Logger
}
