package handlers

import (
	"gateway/internal/config"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"github.com/sirupsen/logrus"
	"net/http"
)

type RedirectI interface {
	RedirectRequest(w http.ResponseWriter, r *http.Request)
	Health(w http.ResponseWriter, r *http.Request)
}

type Handlers struct {
	Redirect RedirectI
}

type RouteDependencies struct {
	Logger logrus.FieldLogger
	//Middlewares Middlewares

	//Otp OtpI

	Redirect RedirectI

	Conf *config.Config
}

type RouteOptions struct {
	Version string
	Cors    RouteOptionsCors
}

type RouteOptionsCors struct {
	AllowedOrigins   []string
	AllowedHeaders   []string
	AllowCredentials bool
	Debug            bool
}

func NewRoute(dep RouteDependencies) http.Handler {
	r := mux.NewRouter()

	r.HandleFunc("/health", dep.Redirect.Health)
	r.PathPrefix("").HandlerFunc(dep.Redirect.RedirectRequest)

	//auth.Use(dep.Middlewares.RequestIDMiddleware.Middleware)
	//profile.Use(dep.Middlewares.AuthMiddleware.Middleware)

	corsHandler := cors.New(cors.Options{
		AllowedOrigins: []string{"*"},
		AllowedMethods: []string{"GET", "POST", "PUT", "DELETE", "OPTIONS"},
		AllowedHeaders: []string{"Origin", "Authorization", "Content-Type"},
	})

	return corsHandler.Handler(r)
}
