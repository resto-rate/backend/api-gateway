package handlers

import (
	"bytes"
	"gateway/internal/config"
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
	"io"
	"mime/multipart"
	"net/http"
	"time"
)

type RedirectHandlers struct {
	logger logrus.FieldLogger
	conf   *config.Config
}

type RedirectDependencies struct {
	Logger logrus.FieldLogger
	Conf   *config.Config
}

func NewRedirectHandlers(dep RedirectDependencies) (*RedirectHandlers, error) {
	if dep.Logger == nil {
		return nil, ErrDepLoggerIsNil
	}
	if dep.Conf == nil {
		return nil, ErrDepConfIsNil
	}

	logger := dep.Logger.WithField("", "RedirectHandlers")

	return &RedirectHandlers{
		conf:   dep.Conf,
		logger: logger,
	}, nil
}

func (o *RedirectHandlers) Health(w http.ResponseWriter, r *http.Request) {
	writeJson(w, nil, "Hello")
}

func (o *RedirectHandlers) RedirectRequest(w http.ResponseWriter, r *http.Request) {
	redirectServiceStr := make([]rune, 0)
	var redirectHostStr string

	viper.SetEnvPrefix("serviceaddresses")
	err := viper.BindEnv("establishments")
	if err != nil {
		o.logger.Errorf("Error sending request: %v", err.Error())
		return
	}

	err = viper.BindEnv("auth")
	if err != nil {
		o.logger.Errorf("Error sending request: %v", err.Error())
		return
	}

	err = viper.BindEnv("notification")
	if err != nil {
		o.logger.Errorf("Error sending request: %v", err.Error())
		return
	}

	for _, v := range r.URL.Path {
		if string(redirectServiceStr) == "/auth" {
			redirectHostStr = viper.GetString("auth")
			break
		}
		if string(redirectServiceStr) == "/establishments" {
			redirectHostStr = viper.GetString("establishments")
			break
		}
		if string(redirectServiceStr) == "/notification" {
			redirectHostStr = viper.GetString("notification")
			break
		}
		redirectServiceStr = append(redirectServiceStr, v)
	}

	client := http.Client{Timeout: time.Second * 10}
	req := &http.Request{}

	resp := &http.Response{}

	file, h, err := r.FormFile("imageFile")
	if err == nil {
		body := &bytes.Buffer{}
		writer := multipart.NewWriter(body)
		fw, err := writer.CreateFormFile("imageFile", h.Filename)
		if err != nil {
			o.logger.Errorf("Error create form file: %v", err.Error())
		}

		_, err = io.Copy(fw, file)
		if err != nil {
			o.logger.Errorf("Error copy file: %v", err.Error())
		}
		err = writer.Close()
		if err != nil {
			o.logger.Errorf("Error close writer: %v", err.Error())
		}

		req, err = http.NewRequest("POST", redirectHostStr+r.URL.Path, bytes.NewReader(body.Bytes()))
		if err != nil {
			o.logger.Errorf("Error creating request: %v", err.Error())
		}
		req.Header.Set("Content-Type", writer.FormDataContentType())
	} else {
		if file == nil {
			req, err = http.NewRequest(r.Method, redirectHostStr+r.URL.Path, r.Body)
			if err != nil {
				o.logger.Errorf("Error creating request: %v", err.Error())
			}
			req.Header.Set("Content-Type", "application/json")
		}
	}

	defer func(file multipart.File) {
		if file == nil {
			return
		}

		err := file.Close()
		if err != nil {
			o.logger.Errorf("Error close file: %v", err.Error())
		}
	}(file)

	req.Header.Set("Authorization", r.Header.Get("Authorization"))
	req.URL.RawQuery = r.URL.Query().Encode()

	resp, err = client.Do(req)
	if err != nil {
		o.logger.Errorf("Error sending request: %v", err.Error())
		w.WriteHeader(http.StatusInternalServerError)
		return
	}
	defer func(Body io.ReadCloser) {
		err := Body.Close()
		if err != nil {
			o.logger.Errorf("Error close file: %v", err.Error())
			return
		}
	}(resp.Body)

	bodyBytes, err := io.ReadAll(resp.Body)
	if err != nil {
		o.logger.Errorf("Error reading response body: %v", err.Error())
		return
	}
	if !(resp.StatusCode >= 200 && resp.StatusCode < 300) {
		w.Write(bodyBytes)
		w.WriteHeader(resp.StatusCode)
		return
	}

	displayRedirectedJson(w, o.logger, bodyBytes, resp.StatusCode)
}
