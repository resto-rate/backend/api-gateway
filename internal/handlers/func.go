package handlers

import (
	"encoding/json"
	"fmt"
	"github.com/sirupsen/logrus"
	"net/http"
)

var (
	ErrDepLoggerIsNil = NewErrDepIsNil("Logger")
	ErrDepConfIsNil   = NewErrDepIsNil("Config")
)

type ErrDepIsNil struct {
	unit string
}

func (err ErrDepIsNil) Error() string {
	return fmt.Sprintf("%s is nil", err.unit)
}

func NewErrDepIsNil(unit string) error {
	return ErrDepIsNil{unit: unit}
}

func writeJson(w http.ResponseWriter, logger logrus.FieldLogger, body interface{}) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")

	jsonBody, err := json.Marshal(body)
	if err != nil {
		logger.WithError(err).Error("json.Marshal in response - error")
	}

	_, err = w.Write(jsonBody)
	if err != nil {
		logger.WithError(err).Error("w.Write in response - error")
	}
}

func displayRedirectedJson(w http.ResponseWriter, logger logrus.FieldLogger, body []uint8, statusCode int) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(statusCode)

	_, err := w.Write(body)
	if err != nil {
		logger.WithError(err).Error("w.Write in response - error")
	}
}
