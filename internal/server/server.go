package server

import (
	"context"
	"gateway/internal/config"
	"github.com/sirupsen/logrus"
	"net/http"
	"time"
)

type Dependencies struct {
	Handler http.Handler
	Logger  logrus.FieldLogger
	Config  *config.Config
}

type Server struct {
	logger  logrus.FieldLogger
	srvHTTP *http.Server

	shutdownTimeout time.Duration
	config          *config.Config
}

func NewServer(dep Dependencies) *Server {
	s := &Server{
		srvHTTP: &http.Server{
			Addr:    ":" + dep.Config.WebServer.ListenPorts.Http,
			Handler: dep.Handler,
		},

		logger:          dep.Logger,
		shutdownTimeout: 5,
		config:          dep.Config,
	}

	return s
}

func (s *Server) Start() error {

	s.logger.Info(">>>>>>> Server started")
	//sentry.CaptureMessage("Server started")

	err := s.srvHTTP.ListenAndServe()
	if err != nil {
		s.logger.Errorf("busy port %w", err)
	}

	return nil
}

func (s *Server) Stop() error {
	var ctx context.Context
	var cancelCtx context.CancelFunc

	if s.shutdownTimeout > 0 {
		ctx, cancelCtx = context.WithTimeout(context.Background(), s.shutdownTimeout)
		defer cancelCtx()
	} else {
		ctx = context.Background()
	}

	//sentry.CaptureMessage("Server stopped")

	err := s.srvHTTP.Shutdown(ctx)
	if err != nil {
		return err
	}

	return nil
}
