package config

import (
	"github.com/sirupsen/logrus"
	"github.com/spf13/viper"
)

const (
	configPath = "."
	configName = "conf"
	logLevel   = 5
)

type Config struct {
	Logger    logger    `yaml:"Logger"`
	WebServer webServer `yaml:"WebServer"`
}

type webServer struct {
	ListenPorts struct {
		Http string `yaml:"Http"`
	} `yaml:"ListenPorts"`
}

type logger struct {
	Level logrus.Level
	Type  string
}

func NewConfiguration() (*Config, error) {
	viper.AddConfigPath(configPath)
	viper.SetConfigName(configName)

	var configuration Config

	err := viper.ReadInConfig()
	if err != nil {
		return nil, err
	}

	err = viper.Unmarshal(&configuration)
	if err != nil {
		return nil, err
	}

	viper.SetEnvPrefix("gateway")
	err = viper.BindEnv("port")
	if err != nil {
		return nil, err
	}

	configuration.WebServer.ListenPorts.Http = viper.GetString("port")

	return &configuration, nil
}
